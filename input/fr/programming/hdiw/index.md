# Comment ça fonctionne ?

Cette section du wiki expliquera comment fonctionne PSDK du côté programmation.
## Index

Cet index n'est pas définitif, nous ajouterons du contenu lorsqu'il nous sera demandé (et écrit), mais à présent, l'index actuel est :

- [Scènes (GamePlay)](fr/programming/hdiw/scenes.md)
- [Système d'UI de PSDK](fr/programming/hdiw/ui.md)