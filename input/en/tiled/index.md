# Tiled Map Editor

In this section, we will talk about **Tiled** software, which is a powerful 2D level editor. With the Yuri's [Tiled2rxdata](https://gitlab.com/NuriYuri/tiled2rxdata) it allows you to create Maps efficiently, with **an infinity of layers**, then converts them to be usable with **PSDK**.

## Liste des articles

- [Install Tiled](install.md)
- [Getting Started](getting_started.md)